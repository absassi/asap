.. Structure conventions
     # with overline, for parts
     * with overline, for chapters
     = for sections
     - for subsections
     ^ for subsubsections
     " for paragraphs

################
Getting Started!
################

.. toctree::
   :maxdepth: 1
   :titlesonly:
   :hidden:

   structure

:doc:`Understanding the project structure <structure>`
------------------------------------------------------
*the project structure follows best practices to implement multi-module projects
that maintain clean dependencies, easily integrate 3rd party libraries and use
cmake in an as simple as possible way. You can learn more about it here.*
