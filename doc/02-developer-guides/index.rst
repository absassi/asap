.. Structure conventions
     # with overline, for parts
     * with overline, for chapters
     = for sections
     - for subsections
     ^ for subsubsections
     " for paragraphs

################
Developer Guides
################

.. toctree::
   :maxdepth: 1
   :titlesonly:
   :hidden:

   build
   devflow
   sphinx-doc


:doc:`Documentation <sphinx-doc>`
---------------------------------
*if you are, and you should be, writing API documentation in your own code or
adding more parts to the user manual of this project, you should read this part.
It will help you understand how this User Manual is maintained and transformed 
into the final HTML output*
